<?php

if (!defined('BASEPATH')) {
  exit('No script access allowed');
}

class M_route extends CI_Model {

  function __construct() {
    parent:: __construct();
  }
  
  function get_data_by_fields($data) {
    return $this->db->where($data)->get('route')->result();
  }
  
	function get_data_by_id($route_ID) {
    return $this->db->where('route_ID', $route_ID)->get('route')->row();
  }
	
  function insert($data) {
    $this->db->insert('route', $data);
  }
  
  function update($code_route, $data) {
    $this->db->where('route_ID', $code_route)->update('route', $data);
  }
  
  function delete($code_route) {
    $this->db->where('route_ID', $code_route)->delete('route');
  }
  
  
}
