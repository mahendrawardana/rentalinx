<?php

if (!defined('BASEPATH')) {
  exit('No script access allowed');
}

class M_city extends CI_Model {

  function __construct() {
    parent:: __construct();
  }
  
  function get_data_by_fields($data) {
    return $this->db->where($data)->get('city')->result();
  }
	
  function get_data_by_id($code_city) {
    return $this->db->where('code_city', $code_city)->get('city')->row();
  }
  
  function insert($data) {
    $this->db->insert('city', $data);
  }
  
  function update($code_city, $data) {
    $this->db->where('code_city', $code_city)->update('city', $data);
  }
  
  function delete($code_city) {
    $this->db->where('code_city', $code_city)->delete('city');
  }
  
  
}
