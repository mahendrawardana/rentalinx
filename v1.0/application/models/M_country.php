<?php

if (!defined('BASEPATH')) {
  exit('No script access allowed');
}

class M_country extends CI_Model {

  function __construct() {
    parent:: __construct();
  }
  
  function get_data_by_fields($data) {
    return $this->db->where($data)->get('country')->result();
  }
  
  function get_data_by_id($code_country) {
    return $this->db->where('code_country', $code_country)->get('country')->row();
  }
	
  function insert($data) {
    $this->db->insert('country', $data);
  }
  
  function update($code_country, $data) {
    $this->db->where('code_country', $code_country)->update('country', $data);
  }
  
  function delete($code_country) {
    $this->db->where('code_country', $code_country)->delete('country');
  }
  
  
}
