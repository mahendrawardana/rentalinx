<?php

if (!defined('BASEPATH')) {
  exit('No script access allowed');
}

class M_state extends CI_Model {

  function __construct() {
    parent:: __construct();
  }
  
  function get_data_by_fields($data) {
    return $this->db->where($data)->get('state')->result();
  }
	
  function get_data_by_id($code_state) {
    return $this->db->where('code_state', $code_state)->get('state')->row();
  }
  
  function insert($data) {
    $this->db->insert('state', $data);
  }
  
  function update($code_state, $data) {
    $this->db->where('code_state', $code_state)->update('state', $data);
  }
  
  function delete($code_state) {
    $this->db->where('code_state', $code_state)->delete('state');
  }
  
  
}
