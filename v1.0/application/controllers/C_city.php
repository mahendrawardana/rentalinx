<?php if ( ! defined('BASEPATH')) {
  exit('No direct script access allowed');
}

class C_city extends CI_Controller {
	function __construct(){
		parent:: __construct();
    $this->load->model('m_city');
	}
  
  function get_data_by_fields() {
    $this->l_rest_method->token_check();
    $get    = $_GET;
    $this->l_base_value->table_field_check($get, 'city');
    $data   = $this->m_city->get_data_by_fields($get);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_get(),
      'count'=>count($data),
      'results'=>$data
    );
    $this->l_rest_method->json_display($responses);
  }
	
  function get_data_by_id($code_city) {
    $this->l_rest_method->token_check();
		$this->l_base_value->update_pk_exist('city', 'code_city', $code_city);
    $data   = $this->m_city->get_data_by_id($code_city);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_get(),
      'results'=>$data
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function insert() {
    $this->l_rest_method->token_check();
    $data    = (array) json_decode(file_get_contents('php://input'));
    $this->l_base_value->table_field_check($data, 'city');
    $this->m_city->insert($data);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_post()
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function update($code_city) {
    $this->l_rest_method->token_check();
		$this->l_base_value->update_pk_exist('city', 'code_city', $code_city);
    $data    = (array) json_decode(file_get_contents('php://input'));
    $this->l_base_value->table_field_check($data, 'city');
    $this->m_city->update($code_city, $data);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_put()
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function delete($code_city) {
    $this->l_rest_method->token_check();
    $this->m_city->delete($code_city);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_delete()
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function metadata() {
    $this->l_rest_method->token_check();
    $data = $this->db->field_data('city');
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_get(),
      'results'=>$data
    );
    $this->l_rest_method->json_display($responses);
  }
  
}