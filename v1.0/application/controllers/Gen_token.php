<?php if ( ! defined('BASEPATH')) {
  exit('No direct script access allowed');
}

class Gen_token extends CI_Controller {
  
	function __construct(){
		parent:: __construct();
	}
	
	function generate_token() {
		$token = openssl_random_pseudo_bytes(32); 
		$token = bin2hex($token);
		echo $token;
	}

}