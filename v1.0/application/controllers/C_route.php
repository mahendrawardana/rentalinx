<?php if ( ! defined('BASEPATH')) {
  exit('No direct script access allowed');
}

class C_route extends CI_Controller {
	function __construct(){
		parent:: __construct();
    $this->load->model('m_route');
	}
  
  function get_data_by_fields() {
    $this->l_rest_method->token_check();
    $get    = $_GET;
    $this->l_base_value->table_field_check($get, 'route');
    $data   = $this->m_route->get_data_by_fields($get);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_get(),
      'count'=>count($data),
      'results'=>$data
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function get_data_by_id($route_ID) {
    $this->l_rest_method->token_check();
		$this->l_base_value->update_pk_exist('route', 'route_ID', $route_ID);
    $data   = $this->m_route->get_data_by_id($route_ID);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_get(),
      'results'=>$data
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function insert() {
    $this->l_rest_method->token_check();
    $data    = (array) json_decode(file_get_contents('php://input'));
    $this->l_base_value->table_field_check($data, 'route');
    $this->m_route->insert($data);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_post()
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function update($route_ID) {
    $this->l_rest_method->token_check();
		$this->l_base_value->update_pk_exist('route', 'route_ID', $route_ID);
    $data    = (array) json_decode(file_get_contents('php://input'));
    $this->l_base_value->table_field_check($data, 'route');
    $this->m_route->update($code_route, $data);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_put()
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function delete($code_route) {
    $this->l_rest_method->token_check();
    $this->m_route->delete($code_route);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_delete()
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function metadata() {
    $this->l_rest_method->token_check();
    $data = $this->db->field_data('route');
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_get(),
      'results'=>$data
    );
    $this->l_rest_method->json_display($responses);
  }
  
}