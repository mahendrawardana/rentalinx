<?php if ( ! defined('BASEPATH')) {
  exit('No direct script access allowed');
}

class C_welcome extends CI_Controller {
	function __construct() {
		parent:: __construct();
	}
  
  function index() {
    $responses = array(
      'status'=>200,
      'message'=>'Selamat datang di API Rentalinx :D',
      'created_at'=>strtotime('16-2-2016'),
      'documentation'=>'rentalinxapi.wordpress.com'
    );
    $this->l_rest_method->json_display($responses);
  }
  
}