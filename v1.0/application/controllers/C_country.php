<?php if ( ! defined('BASEPATH')) {
  exit('No direct script access allowed');
}

class C_country extends CI_Controller {
	function __construct(){
		parent:: __construct();
    $this->load->model('m_country');
	}
  
  function get_data_by_fields() {
    $this->l_rest_method->token_check();
    $get    = $_GET;
    $this->l_base_value->table_field_check($get, 'country');
    $data   = $this->m_country->get_data_by_fields($get);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_get(),
      'count'=>count($data),
      'results'=>$data
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function get_data_by_id($code_country) {
    $this->l_rest_method->token_check();
		$this->l_base_value->update_pk_exist('country', 'code_country', $code_country);
    $data   = $this->m_country->get_data_by_id($code_country);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_get(),
      'results'=>$data
    );
    $this->l_rest_method->json_display($responses);
  }
	
  function insert() {
    $this->l_rest_method->token_check();
    $data    = (array) json_decode(file_get_contents('php://input'));
    $this->l_base_value->table_field_check($data, 'country');
    $this->m_country->insert($data);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_post()
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function update($code_country) {
    $this->l_rest_method->token_check();
		$this->l_base_value->update_pk_exist('country', 'code_country', $code_country);
    $data    = (array) json_decode(file_get_contents('php://input'));
    $this->l_base_value->table_field_check($data, 'country');
    $this->m_country->update($code_country, $data);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_put()
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function delete($code_country) {
    $this->l_rest_method->token_check();
    $this->m_country->delete($code_country);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_delete()
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function metadata() {
    $this->l_rest_method->token_check();
    $data = $this->db->field_data('country');
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_get(),
      'results'=>$data
    );
    $this->l_rest_method->json_display($responses);
  }
  
}