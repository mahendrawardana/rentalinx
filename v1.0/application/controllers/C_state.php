<?php if ( ! defined('BASEPATH')) {
  exit('No direct script access allowed');
}

class C_state extends CI_Controller {
	function __construct(){
		parent:: __construct();
    $this->load->model('m_state');
	}
  
  function get_data_by_fields() {
    $this->l_rest_method->token_check();
    $get    = $_GET;
    $this->l_base_value->table_field_check($get, 'state');
    $data   = $this->m_state->get_data_by_fields($get);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_get(),
      'count'=>count($data),
      'results'=>$data
    );
    $this->l_rest_method->json_display($responses);
  }
	
  function get_data_by_id($code_state) {
    $this->l_rest_method->token_check();
		$this->l_base_value->update_pk_exist('state', 'code_state', $code_state);
    $data   = $this->m_state->get_data_by_id($code_state);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_get(),
      'results'=>$data
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function insert() {
    $this->l_rest_method->token_check();
    $data    = (array) json_decode(file_get_contents('php://input'));
    $this->l_base_value->table_field_check($data, 'state');
    $this->m_state->insert($data);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_post()
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function update($code_state) {
    $this->l_rest_method->token_check();
    $data    = (array) json_decode(file_get_contents('php://input'));
    $this->l_base_value->table_field_check($data, 'state');
    $this->m_state->update($code_state, $data);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_put()
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function delete($code_state) {
    $this->l_rest_method->token_check();
    $data    = (array) json_decode(file_get_contents('php://input'));
    $this->l_base_value->table_field_check($data, 'state');
    $this->m_state->delete($code_state);
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_delete()
    );
    $this->l_rest_method->json_display($responses);
  }
  
  function metadata() {
    $this->l_rest_method->token_check();
    $data = $this->db->field_data('state');
    $responses = array(
      'status'=>200,
      'message'=>$this->l_base_value->message_get(),
      'results'=>$data
    );
    $this->l_rest_method->json_display($responses);
  }
  
}