<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes with
| underscores in the controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'c_welcome';

/**
 * Di bawah ini merupakan routes untuk API
 */

// KOTA APIs
$route['kota.json?(:any)']['GET']                          = 'c_city/get_data_by_fields/$1';
$route['kota/(:any).json']['GET']                          = 'c_city/get_data_by_id/$1';
$route['kota.json']['POST']                                = 'c_city/insert';
$route['kota/(:any).json']['PUT']                          = 'c_city/update/$1';
$route['kota/(:any).json']['DELETE']                       = 'c_city/delete/$1';
$route['kota/metadata.json']['GET']                        = 'c_city/metadata';

// STATE APIs
$route['provinsi.json?(:any)']['GET']                         = 'c_state/get_data_by_fields/$1';
$route['provinsi/(:any).json']['GET']                         = 'c_state/get_data_by_id/$1';
$route['provinsi.json']['POST']                               = 'c_state/insert';
$route['provinsi/(:any).json']['PUT']                         = 'c_state/update/$1';
$route['provinsi/(:any).json']['DELETE']                      = 'c_state/delete/$1';
$route['provinsi/metadata.json']['GET']                       = 'c_state/metadata';

// COUNTRY APIs
$route['negara.json?(:any)']['GET']                       = 'c_country/get_data_by_fields/$1';
$route['negara/(:any).json']['GET']                       = 'c_country/get_data_by_id/$1';
$route['negara.json']['POST']                             = 'c_country/insert';
$route['negara/(:any).json']['PUT']                       = 'c_country/update/$1';
$route['negara/(:any).json']['DELETE']                    = 'c_country/delete/$1';
$route['negara/metadata.json']['GET']                     = 'c_country/metadata';

// ROUTE APIs
$route['rute.json?(:any)']['GET']                         = 'c_route/get_data_by_fields/$1';
$route['rute/(:any).json']['GET']                         = 'c_route/get_data_by_id/$1';
$route['rute.json']['POST']                               = 'c_route/insert';
$route['rute/(:any).json']['PUT']                         = 'c_route/update/$1';
$route['rute/(:any).json']['DELETE']                      = 'c_route/delete/$1';
$route['rute/metadata.json']['GET']                       = 'c_route/metadata';

// CAR MANUFACTURE APIs
$route['car_manufacture.json?(:any)']['GET']               = 'c_car_manufacture/get_data_by_fields/$1';
$route['car_manufacture/(:any).json']['GET']               = 'c_car_manufacture/get_data_by_id/$1';
$route['car_manufacture.json']['POST']                     = 'c_car_manufacture/insert';
$route['car_manufacture/(:any).json']['PUT']               = 'c_car_manufacture/update/$1';
$route['car_manufacture/(:any).json']['DELETE']            = 'c_car_manufacture/delete/$1';
$route['car_manufacture/metadata.json']['GET']             = 'c_car_manufacture/metadata';

// 

$route['404_override'] = 'c_welcome';

$route['translate_uri_dashes'] = FALSE; // Digunakan untuk mentranslate URI yang berisi '-' menjadi '_' karena class dan method tidak boleh berisikan Dash
