<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class L_base_value {

		public function token_expired() {
			return 'Akun anda sudah expired, silahkan hubungi Operator';
		}
    
    public function message_get() {
      return 'Data berhasil didapat';
    }
    
    public function message_post() {
      return 'Data berhasil disimpan';
    }
    
    public function message_put() {
      return 'Data berhasil diperbarui';
    }
    
    public function message_delete() {
      return 'Data berhasil dihapus';
    }
    
    public function token_list() {
      $token = array(
                  '284ad63bbe7b06efc5e75860e006efb1568565bf1271f171cdafdb8ca4333809',
                  '0228aa877d8f54906d7555b7f38fa9cd37839ea644ffb46d919843283c479406',
                  '4fae93168286e205b814996c6038c7986c16239e25f288c241598d443b04dda2',
                  '9db7acbfa8f6d15b0d6ac49d350adbb5b1a4291c8bf2dddc2b96a0d46303e495',
                  'b43c2a835424a07853284ab674392fd059ad33abda7cde0bcdcd93add02064e6'
               );
      return $token;
    }
		
    public function filter_val() {
      $filter = array(
        'sort'=>'Define (ASC/DESC) & Field sortir dipilih pada parameter pertama',
        'fields'=>'Format Field diberikan koma i.e fields=nim,nama,kelas'
      );
      return $filter;
    }
    
    public function table_field_check($fields, $table_name) {
      $ci =& get_instance();
      $ci->load->library('l_rest_method');
      if($_SERVER['REQUEST_METHOD'] === 'POST' || $_SERVER['REQUEST_METHOD'] === 'PUT') {
        if(count($fields) == 0) { // Jika POST Value yang dikirim melalui method POST kosong
          $response = array(
            'status'=>400,
            'message'=>'POST value kosong',
          );
          $ci->l_rest_method->json_display($response);
        } else { // Jika POST Value yang dikirim terisi akan dilakukan pengecekan field dengan field  yang tersedia di database
          foreach($fields as $field=>$value) {
            if(!$ci->db->field_exists($field, $table_name)) {
              $response = array(
                'status' => 400,
                'message' => 'Field request tidak tersedia',
                'fields' => $ci->db->list_fields($table_name)
              );
              $ci->l_rest_method->json_display($response);
              return FALSE;
            }
          }
        }
      } elseif(count($fields) != 0) {
        foreach($fields as $field=>$value) {
          if(!$ci->db->field_exists($field, $table_name)) {
            $response = array(
              'status' => 400,
              'message' => 'Field request tidak tersedia',
              'fields' => $ci->db->list_fields($table_name)
            );
            $ci->l_rest_method->json_display($response);
            return FALSE;
          }
        }
      }
    }
		
		public function update_pk_exist($table, $pk, $pk_val) {
      $ci =& get_instance();
			$num = $ci->db->where($pk, $pk_val)->get($table)->num_rows();
			if($num == 0) {
	      $ci->load->library('l_rest_method');
				$responses = array(
					'status'=>404,
					'message'=>'Data ID field tidak tersedia'
				);
				$ci->l_rest_method->json_display($responses);
				return FALSE;
			}
		}
    
    public function params_filter($get) {
      $filter = NULL;
      foreach($this->filter_val() as $key=>$val) {
        if(isset($get[$key])) {
          $filter[$key] = $get[$key];
          unset($get[$key]);
          if($key == 'sort' ) {
            $filter['field_sort'] = key($get);
          }
        }
      }
      $responses = array(
        'filter'=>$filter,
        'clear'=>$get
      );
      return $responses;
    }
    
    
    
    
    
    
		public function db_config() {
  		$config['hostname']   = 'localhost';
			$config['username']   = 'root';
			$config['password']   = '';
			$config['database']   = 'pilar_emkl';
			$config['dbdriver']   = 'mysqli';
			$config['dbprefix']   = '';
			$config['pconnect']   = FALSE;
			$config['db_debug']   = TRUE;
			return $config;
		}
		
		public function db_config_ol() {
  		$config['hostname'] = 'localhost';
			$config['username']   = 'software-emkl';
			$config['password']   = 'jmUdbeBs';
			$config['database']   = 'demoemkl';
			$config['dbdriver']   = 'mysqli';
			$config['dbprefix']   = '';
			$config['pconnect']   = FALSE;
			$config['db_debug']   = TRUE;
			return $config;
  	}
		
		
}

