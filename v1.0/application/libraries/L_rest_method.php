<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class L_rest_method {

	public function json_display($response) {
		$ci =& get_instance();
		$ci->output->set_status_header(200)
							 ->set_content_type('application/json', 'utf-8')
							 ->set_output(json_encode($response, JSON_PRETTY_PRINT))
							 ->_display();
		exit;
	}
  
  public function token_check() {
    $ci =& get_instance();
    $ci->load->library('l_base_value');
		// Get Data form Headers
		$headers   = apache_request_headers();
		$token     = isset($headers['Token'])?$headers['Token']:'';
		// Execution
    if(empty($token)) {
      $responses = array(
        'status'=>401,
        'message'=>'Token diperlukan'
      );
      $this->json_display($responses);
      return FALSE;
    } elseif(!in_array($token, $ci->l_base_value->token_list())) {
      $responses = array(
        'status'=>401,
        'message'=> 'Token tidak terdaftar'
      );
      $this->json_display($responses);
      return FALSE;
		}
    
	}
		
}

